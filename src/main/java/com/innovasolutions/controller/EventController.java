package com.innovasolutions.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.innovasolutions.facotry.EventProcessorFactory;
import com.innovasolutions.model.Event;
import com.innovasolutions.services.EventProcessor;
import com.innovasolutions.util.Utility;


@RestController
public class EventController {
	
    private static final Logger logger = LoggerFactory.getLogger(EventController.class);

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody Event event) throws Exception {

    	if(null==event)
    	{
    		return null;
    	}
    	logger.info(event.toString());
    	
		try 
		{
			if (Utility.EVENT_MESSAGE.DISPATCH.toString().equalsIgnoreCase(event.getEventMessage())) {
				if (Utility.EVENT_TYPE.PUT.toString().equalsIgnoreCase(event.getEventName())) {

					EventProcessor processor = EventProcessorFactory.getProcessor(event.getEventMessage(),
							event.getBucketName(), event.getFileName(), event.getAwsRegion(), event.getConfigurationId());

					processor.process();

				}

			}
		}
    	catch(Exception e)
    	{
    		logger.info(e.getMessage());
    		throw e;
    	}
    	
    	
        return "Successfull created resource";

    }


}
