package com.innovasolutions.facotry;

import com.innovasolutions.serviceImpl.DynamoDBProcessor;
import com.innovasolutions.serviceImpl.SNSProcessor;
import com.innovasolutions.services.EventProcessor;
import com.innovasolutions.util.Utility;

public class EventProcessorFactory {

	
	public static EventProcessor getProcessor(String eventMessage,String bucketName, String fileName, String region, String configurationId) throws Exception
	{
		
		if(Utility.EVENT_MESSAGE.DISPATCH.toString().equalsIgnoreCase(eventMessage))
		{
			return new DynamoDBProcessor(bucketName,fileName,region,configurationId);
		}
		if(Utility.EVENT_MESSAGE.PUBLISH.toString().equalsIgnoreCase(eventMessage))
		{
			return new SNSProcessor(bucketName,fileName,region);
		}
		
		throw new Exception("Invalid Message Type");
	}
}
