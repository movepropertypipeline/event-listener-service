package com.innovasolutions.model;

import java.util.List;

/**
 * 
 * 
 * @author shobhit
 *
 *
 *{
  "Records": [
    {
      "eventVersion": "2.0",
      "eventSource": "aws:s3",
      "awsRegion": "ap-south-1",
      "eventTime": "2018-05-17T06:49:53.717Z",
      "eventName": "ObjectCreated:Put",
      "userIdentity": {
        "principalId": "AWS:AIDAJE6TZYLWENPBK3OAE"
      },
      "requestParameters": {
        "sourceIPAddress": "182.76.162.174"
      },
      "responseElements": {
        "x-amz-request-id": "9BB13C3DD4278F55",
        "x-amz-id-2": "R7XMDjBNavB9OLAOjT1HXZK/z3Eoxqn63eFUhOKUZA+N3Nj13uKM+UpKx0RK9Nrcefn2qrAkovA="
      },
      "s3": {
        "s3SchemaVersion": "1.0",
        "configurationId": "e9dfdd6d-7b06-4638-b78c-3c6c0894ff67",
        "bucket": {
          "name": "poc-bijoshtj",
          "ownerIdentity": {
            "principalId": "A350J4COOJWOEF"
          },
          "arn": "arn:aws:s3:::poc-bijoshtj"
        },
        "object": {
          "key": "bengaluru/2018/05/s3_test.json",
          "size": 28,
          "eTag": "ada5f1f8cf56cb09f7b0b8f19ad89a20",
          "sequencer": "005AFD2611ABF9E287"
        }
      }
    }
  ]
}
 */
public class Event {

	private String awsRegion;
	
	private String eventName;
	
	private String bucketName;
	
	private String fileName;

	private String eventMessage;

	private String configurationId;



	public String getAwsRegion() {
		return awsRegion;
	}

	public void setAwsRegion(String awsRegion) {
		this.awsRegion = awsRegion;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String files) {
		this.fileName = files;
	}

	public String getEventMessage() {
		
		return this.eventMessage;
	}

	public void setEventMessage(String eventMessage) {
		this.eventMessage = eventMessage;
	}

	public void setConfigurationId(String configurationId) {
		
		this.configurationId = configurationId;
	}

	public String getConfigurationId() {
		return configurationId;
	}
}
