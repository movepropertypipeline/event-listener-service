package com.innovasolutions.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {

        "com.innovasolutions.controller",

        "com.innovasolutions.Model",

    })
public class EventlistenerserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventlistenerserviceApplication.class, args);
	}
}
