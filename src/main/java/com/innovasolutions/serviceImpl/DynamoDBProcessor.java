package com.innovasolutions.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketNotificationConfiguration;
import com.amazonaws.services.s3.model.Filter;
import com.amazonaws.services.s3.model.FilterRule;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.NotificationConfiguration;
import com.amazonaws.services.s3.model.S3KeyFilter;
import com.amazonaws.services.s3.model.S3KeyFilter.FilterRuleName;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.innovasolutions.services.EventProcessor;
import com.innovasolutions.util.Utility;

public class DynamoDBProcessor implements EventProcessor {

	Logger logger = LoggerFactory.getLogger(DynamoDBProcessor.class);
	
	private String bucketName;
	private String fileName;
	private String region;

	private String configurationId;

	public DynamoDBProcessor(String bucketName, String fileName, String region,String configurationId) {
		this.bucketName = bucketName;
		this.fileName = fileName;
		this.region = region;
		this.configurationId = configurationId;
	}

	// we can create our own custom exception corresponding to each processor with proper messages
	@Override
	public void process() throws Exception {
		
		// establish a connection with S3 and then check for each file existence in the given bucket from given list
		// if exist insert information in dynamoDB table
		
		 AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
		
		// bucket conf can be used to extract all notification configuration of s3 bucket info like prefix,postfix ,arn etc
			BucketNotificationConfiguration bucketConf = s3.getBucketNotificationConfiguration(this.bucketName);
			NotificationConfiguration notificationConf = bucketConf.getConfigurations().get(this.configurationId);
			
			Filter filters = notificationConf==null?null:notificationConf.getFilter();
			
			
			String tableName = Utility.bucketNameToTableNameMap.get(this.bucketName);
			
			List<String> filteredFiles = getFilteredFiles(filters,s3);
			if(filteredFiles.isEmpty())
			{
				filteredFiles.add(this.fileName);
			}
			
			List<Item> items = new ArrayList<Item>();
		for(String key:filteredFiles)
		{
	    	if(!s3.doesObjectExist(this.bucketName,key))
			{
				return;
			}
			System.out.println(s3.getObject(this.bucketName,key).toString());
			prepareBatch(tableName,key,items);
		}
		
		insertBatch(tableName,items);
	}



	


	/**
	 * @param filters
	 * @param s3
	 * @return
	 * 
	 * filter all files satisfying the filter criteria and insert the metadata into dynamo db this is being done so that files for which event is not generated
	 * can be considered also
	 */
	private List<String> getFilteredFiles(Filter filters,AmazonS3 s3) {
		if(null == filters || filters.getS3KeyFilter()==null)
		{
			return new ArrayList<String>();
		}
		List<String> res = new  ArrayList<>();
		ListObjectsV2Result result = null;// s3.listObjectsV2(bucketName);
		ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(this.bucketName);
		S3KeyFilter s3Filter = filters.getS3KeyFilter();
		String prefixValue = "";
		String postFixValue = "";
		for(FilterRule filterRule:s3Filter.getFilterRules())
		{
			if(filterRule.getName().equalsIgnoreCase(FilterRuleName.Prefix.name()))
			{
				prefixValue = filterRule.getValue();
			}
			else
			{
				postFixValue = filterRule.getValue();
			}
		}
		if(prefixValue!=null)
		{
			request = request.withPrefix(prefixValue);
		}
		
		result = s3.listObjectsV2(request);
		
		for(S3ObjectSummary s3Object:result.getObjectSummaries())
		{
			String key = s3Object.getKey();
			if(postFixValue == "")
			{
				res.add(key);
			}
			else if(key.endsWith(postFixValue))
			{
				res.add(key);
			}
		}
		return res;
	}

	/**
	 * @param tableName
	 * @param key
	 */
	private void prepareBatch(String tableName, String key, List<Item> items) {
		

		
		long currentTime = System.currentTimeMillis();

		
		Item item = new Item().withPrimaryKey(new PrimaryKey("bucketname", this.bucketName, "filename", key))
				.withNumber("creationTime", currentTime);

		items.add(item);
		
	}
	/**
	 * @param tableName
	 * @param items
	 */
	private void insertBatch(String tableName, List<Item> items) 
	{
		
		TableWriteItems forumTableWriteItems = new TableWriteItems(tableName) // Forum
				.withItemsToPut(items);

		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(this.region).build();

		DynamoDB dynamoDB = new DynamoDB(client);

		try 
		{
			BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(forumTableWriteItems);

			do {

				// Check for unprocessed keys which could happen if you exceed
				// provisioned throughput

				Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();

				if (outcome.getUnprocessedItems().size() == 0) {
					System.out.println("No unprocessed items found");
				} else {
					System.out.println("Retrieving the unprocessed items");
					outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
				}

			} while (outcome.getUnprocessedItems().size() > 0);
		} 
		catch (Exception e) {
			logger.info("Failed to insert record due to error:" + e.getMessage());
			throw e;
		}
	}
}
