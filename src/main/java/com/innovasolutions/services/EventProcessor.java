package com.innovasolutions.services;

public interface EventProcessor {

	public void process() throws Exception;
}
