package com.innovasolutions.util;

import java.util.HashMap;
import java.util.Map;

public class Utility {

	public enum EVENT_TYPE {
		PUT("ObjectCreated:Put"),
		DELETE("ObjectRemoved:DeleteMarkerCreated");
		
		private final String name;       

	    private EVENT_TYPE(String s) {
	        name = s;
	    }

	    public String toString() {
	        return this.name;
	     }

	};
	
	public enum EVENT_MESSAGE{DISPATCH,RETRIEVE,PUBLISH}
	
	public static Map<String,String> bucketNameToTableNameMap  = new HashMap<String,String>();
	
	static {
		
		bucketNameToTableNameMap.put("innovashob", "droppedfilesinfo");
	}
}
