package com.innovasolutions.server.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.innovasolutions.model.Event;
import com.innovasolutions.util.Utility;

public class ClientPostTest {

    /**

     * Setting up logger

     */

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientPostTest.class);

    public static void main(String[] args) throws IOException

    {
        try

        {

   

            String uri = new String("http://localhost:8080/update");
            
            JSONObject js = new JSONObject();
            JSONArray ja = new JSONArray();
            ja.put("bengaluru/2018/may/01/file3.csv");
            js.put("awsRegion", "us-east-2");
            js.put("bucketName", "innovashob");
            js.put("eventName", Utility.EVENT_TYPE.PUT.toString());
            js.put("eventMessage", Utility.EVENT_MESSAGE.DISPATCH.toString());
            js.put("configurationId", "1b3f7f60-8c92-4617-88a0-5f4eb4ed79be");
            

			try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

				HttpPost putRequest = new HttpPost("http://localhost:8080/update");

				StringEntity input = new StringEntity(js.toString());
				input.setContentType("application/json");
				putRequest.setEntity(input);
				httpClient.execute(putRequest);
				
			} catch (Exception e) {
				LOGGER.error("error:  " + e.getMessage());
				throw e;
			}

        }

        catch (Exception e)

        {

            /**

             *

             * If we get a HTTP Exception display the error message

             */

            LOGGER.error("error:  " + e.getMessage());
          

        }

  

    }

}
